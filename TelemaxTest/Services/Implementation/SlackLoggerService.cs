﻿using System;
using log4net;
using TelemaxTest.Services.Interface;

namespace TelemaxTest.Services.Implementation
{
    public class SlackLoggerService : ILoggerService
    {
        private static ILog log;

        public SlackLoggerService()
        {
            log = LogManager.GetLogger("SlackLogger");
            log4net.Config.XmlConfigurator.Configure();
        }

        #region ILog functions
        public void Debug(string message)
        {
            log.Debug(message);
        }

        public void Debug(string message, Exception ex)
        {
            log.Debug(message, ex);
        }

        public void Error(string message)
        {
            log.Error(message);
        }

        public void Error(string message, Exception ex)
        {
            log.Error(message, ex);
        }

        public void Fatal(string message)
        {
            log.Fatal(message);
        }

        public void Fatal(string message, Exception ex)
        {
            log.Fatal(message, ex);
        }

        public void Info(string message)
        {
            log.Info(message);
        }

        public void Info(string message, Exception ex)
        {
            log.Info(message, ex);
        }

        public void Warn(string message)
        {
            log.Warn(message);
        }

        public void Warn(string message, Exception ex)
        {
            log.Warn(message, ex);
        }
        #endregion
    }
}
