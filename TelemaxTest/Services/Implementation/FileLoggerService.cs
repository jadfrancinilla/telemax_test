﻿using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository;
using System;
using System.Linq;
using TelemaxTest.Services.Interface;

namespace TelemaxTest.Services.Implementation
{
    public class FileLoggerService : IFileLoggerService
    {
        private ILog log;
        private ILoggerService rootLogger;

        public FileLoggerService()
        {
            log = LogManager.GetLogger("FileLogger");
            log4net.Config.XmlConfigurator.Configure();
            rootLogger = new RootLoggerService();
        }

        public void AddFileAppender(string appenderName, string filePath, bool resetToDefaultIfInvalid = true)
        {
            var isValid = CheckIfAppenderIsValid(appenderName, filePath);

            if (!resetToDefaultIfInvalid) {
                throw new ArgumentException("Appender name or path is invalid.");
            }

            if (!isValid) //If resetting to default if invalid is true, and params are invalid, reset to default.
            {
                appenderName = "DefaultLogFileAppender";
                filePath = "C:\\Users\\New User\\Desktop\\default.log"; //Default log file location. Change as needed (per machine)
            }

            var fileAppender = CreateFileAppender(appenderName, filePath);
            ((log4net.Repository.Hierarchy.Logger)log.Logger).AddAppender(fileAppender);

            LogInfoToDefault(string.Format("Added new file appender {0} (saving on file {1})", appenderName, filePath));
        }

        public void SetFilePath(string appenderName, string filePath)
        {
            var appenderExists = CheckIfAppenderExists(appenderName);

            if (!appenderExists)
            {
                throw new ArgumentException("Appender does not exist.");
            }

            var appenderIsValid = CheckIfAppenderIsValid(appenderName, filePath);

            if (!appenderIsValid)
            {
                throw new ArgumentException("Appender name or path is invalid.");
            }

            ILoggerRepository repository = LogManager.GetRepository();
            IAppender[] appenders = repository.GetAppenders();

            log.Info(string.Format("Log file target for appender {0} will be changed to: {1}", appenderName, filePath));

            foreach (IAppender appender in appenders)
            {
                if (appender.Name.CompareTo(appenderName) == 0 && appender is FileAppender)
                {
                    FileAppender fileAppender = (FileAppender)appender;
                    fileAppender.File = filePath;
                    fileAppender.ActivateOptions();
                }
            }

            log.Info(string.Format("Log file target for appender {0} successfully changed to: {1}", appenderName, filePath));
        }
        
        #region Private / helper functions
        private IAppender CreateFileAppender(string appenderName, string filePath)
        {
            RollingFileAppender appender = new RollingFileAppender();
            appender.Name = appenderName;
            appender.File = filePath;
            appender.AppendToFile = true;
            appender.LockingModel = new FileAppender.MinimalLock();
            appender.RollingStyle = RollingFileAppender.RollingMode.Size;
            appender.MaxSizeRollBackups = 10;
            appender.MaximumFileSize = "1MB";

            PatternLayout layout = new PatternLayout();
            layout.ConversionPattern = "%date [%thread] %level %logger - %message%newline";
            layout.ActivateOptions();

            appender.Layout = layout;
            appender.ActivateOptions();

            return appender;
        }

        private void LogInfoToDefault(string message)
        {
            rootLogger.Info(message);
        }

        private bool CheckIfAppenderExists(string appenderName)
        {
            ILoggerRepository repository = LogManager.GetRepository();
            IAppender[] appenders = repository.GetAppenders();
            string[] appenderNames = appenders.Select(a => a.Name).ToArray();

            return appenderNames.Contains(appenderName);
        }

        private bool CheckIfAppenderIsValid(string appenderName, string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath) || (string.IsNullOrWhiteSpace(appenderName) || appenderName == "DefaultLogFileAppender"))
            {
                return false;
            }

            return true;
        }
        #endregion

        #region ILog functions
        public void Debug(string message)
        {
            log.Debug(message);
        }

        public void Debug(string message, Exception ex)
        {
            log.Debug(message, ex);
        }

        public void Error(string message)
        {
            log.Error(message);
        }

        public void Error(string message, Exception ex)
        {
            log.Error(message, ex);
        }

        public void Fatal(string message)
        {
            log.Fatal(message);
        }

        public void Fatal(string message, Exception ex)
        {
            log.Fatal(message, ex);
        }

        public void Info(string message)
        {
            log.Info(message);
        }

        public void Info(string message, Exception ex)
        {
            log.Info(message, ex);
        }

        public void Warn(string message)
        {
            log.Warn(message);
        }

        public void Warn(string message, Exception ex)
        {
            log.Warn(message, ex);
        }
        #endregion
    }
}
