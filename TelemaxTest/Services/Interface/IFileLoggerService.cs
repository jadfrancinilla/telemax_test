﻿namespace TelemaxTest.Services.Interface
{
    public interface IFileLoggerService : ILoggerService
    {
        void AddFileAppender(string appenderName, string filePath, bool resetToDefaultIfInvalid = true);
        void SetFilePath(string appenderName, string filePath);
    }
}
