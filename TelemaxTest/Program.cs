using System;
using TelemaxTest.Services.Implementation;

namespace TelemaxTest
{
    class Program
    {
        static FileLoggerService fileLogger = new FileLoggerService();
        static SlackLoggerService slackLogger = new SlackLoggerService();
        static TelegramLoggerService telegramLogger = new TelegramLoggerService();

        static void Main(string[] args)
        {
            try
            {
                AddNewFileAppender();
                TestOperation(2);
                TestOperation(5);
                CritialOperation(2);
                CritialOperation(1230);

                ChangeFileAppenderPath();
                DebugOperation();

                throw new Exception("Some exception here");
            }

            catch (Exception ex)
            {
                var message = "An error has occurred in the application.";

                slackLogger.Error(message, ex);
                telegramLogger.Error(message, ex);

                Console.ReadLine();
            }
        }

        private static void AddNewFileAppender()
        {
            try
            {
                var appenderName = string.Empty;
                var filePath = string.Empty;

                Console.WriteLine("Enter appender name: ");
                appenderName = Console.ReadLine();

                Console.WriteLine("Enter absolute file path: ");
                filePath = Console.ReadLine();

                fileLogger.AddFileAppender(appenderName, filePath);
            }
            catch (Exception ex)
            {
                fileLogger.Warn(ex.Message, ex);
            }
        }

        private static void ChangeFileAppenderPath()
        {
            try
            {
                var appenderName = string.Empty;
                var filePath = string.Empty; //Default value

                Console.WriteLine("Enter existing appender name to update: ");
                appenderName = Console.ReadLine();

                Console.WriteLine("Enter new absolute file path for appender: ");
                filePath = Console.ReadLine();

                fileLogger.SetFilePath(appenderName, filePath);
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex.Message, ex);
            }
        }

        private static void DebugOperation()
        {
            var message = "This is a test.";

            fileLogger.Debug(message);
            slackLogger.Debug(message);
            telegramLogger.Debug(message);
        }

        private static void TestOperation(int result)
        {
            try
            {
                var someOperation = 1 + 1;

                if(result != someOperation)
                {
                    throw new Exception("Input was not the expected result.");
                }

                fileLogger.Info("Operation successful.");
            }
            catch(Exception ex)
            {
                var errorMessage = string.Format("Operation failed. Expected value was 2, got {0}", result);

                fileLogger.Error(errorMessage, ex);
            }
        }

        private static void CritialOperation(int result)
        {
            try
            {
                var someOperation = 1 + 1;
                if(result != someOperation)
                {
                    throw new Exception("CRITICAL ERROR: Input was not the expected result.");
                }

                var message = "Operation successful.";

                fileLogger.Info(message);
                slackLogger.Info(message);
                telegramLogger.Info(message);
            }
            catch(Exception ex)
            {
                var errorMessage = string.Format("Operation failed. Expected value was 2, got {0}", result);

                fileLogger.Fatal(errorMessage, ex);
                slackLogger.Fatal(errorMessage, ex);
                telegramLogger.Fatal(errorMessage, ex);
            }
        }
    }
}
